<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class FieldsBuildHelper
{
    static function getLimitText (string|null $text, int $limit = 200): string|null
    {
        return $text ? Str::of($text)->limit($limit, '...') : null;
    }
}
