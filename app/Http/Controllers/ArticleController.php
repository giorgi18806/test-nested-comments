<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

//use Yajra\DataTables\DataTables;

class ArticleController extends Controller
{
//    use Datatables;

    public function index()
    {
        $articles = Article::detail()->get();
        return view('articles.index', compact('articles'));
//        return $dataTable->render('articles.index');
//        return Datatables::of(Article::query())->make(true);
    }

    public function show(Article $article): View
    {
        return view('articles.show', compact('article'));
    }
}
