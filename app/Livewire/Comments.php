<?php

namespace App\Livewire;

use App\Livewire\Forms\CommentForm;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Livewire\Attributes\Computed;
use Livewire\Component;

class Comments extends Component
{
    public CommentForm $form;
    public ?Model $model;

    public function mount(Model $model): void
    {
        $this->model = $model;
        $this->form->setModel($model);
    }

    public function answer(?int $id = null): void
    {
        $this->form->setCommentId($id);
        $this->dispatch('change-focus');
    }

    public function cancel(): void
    {
        $this->form->cancel();
    }

    public function store(): void
    {
        $this->form->store();
    }

    #[Computed]
    public function comments(): Collection
    {
        return $this->model->comments()
            ->whereNull('comment_id')
            ->with(['user', 'comments', 'comments.user'])
            ->get();
    }

    public function render(): View
    {
        return view('livewire.comments', [
            'comments' => $this->comments()
        ]);
    }
}
