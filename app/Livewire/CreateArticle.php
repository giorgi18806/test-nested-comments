<?php

namespace App\Livewire;

use App\Livewire\Forms\ArticleForm;
use Livewire\Component;

class CreateArticle extends Component
{
    public ArticleForm $form;

    public function newArticleForm(): void
    {
        $this->articleForm = !$this->articleForm;
    }

    public function save(): void
    {
        $this->form->store();
        $this->dispatch('article-created');
    }

    public function render()
    {
        return view('livewire.create-article');
    }
}
