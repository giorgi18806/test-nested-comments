<?php

namespace App\Livewire\Forms;

use App\Models\Article;
use App\Models\User;
use Illuminate\Support\Str;
use Livewire\Attributes\Validate;
use Livewire\Form;

class ArticleForm extends Form
{
    #[Validate('required|min:3')]
    public string $title = '';

    #[Validate('required|min:3')]
    public string $description = '';

    public int $checkbox = 0;

    public function store()
    {
        if($this->checkbox) {
            $new_article = Article::factory()->create([
                'is_published' => 1,
                'created_at' => now(),
            ]);

            return;
        }
        $this->validate();

        Article::create([
            'title' => $this->title,
            'author_id' => User::inRandomOrder()->first()->id,
            'description' => $this->description,
            'slug' => Str::slug($this->title),
            'is_published' => 1,
            'created_at' => now(),
        ]);

    }
}
