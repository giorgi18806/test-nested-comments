<?php

namespace App\Livewire\Forms;

use App\Rules\ReCaptcha;
use Illuminate\Database\Eloquent\Model;
use Livewire\Attributes\Validate;
use Livewire\Form;

class CommentForm extends Form
{
    public ?Model $model;
    public ?int $commentId = null;
    
    public string $message = '';

    public function setModel(Model $model): void
    {
        $this->model = $model;
    }

    public function setCommentId(?int $commentId = null): void
    {
        $this->commentId = $commentId;
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(): void
    {
        $rules = [
            'message' => 'required|string|min:3',
        ];
        if (env('APP_ENV') == 'production') {
            $rules['g-recaptcha-response'] = ['required', new ReCaptcha];
        }
        $this->validate($rules);

        $this->model->comments()->create([
            'user_id' => fake()->randomDigitNotNull(),
            'comment_id' => $this->commentId,
            'text' => $this->message,
        ]);

        $this->reset(['message', 'commentId']);
    }

    public function cancel(): void
    {
        $this->reset(['message', 'commentId']);
    }
}
