<?php

namespace App\Livewire;

use App\Models\Article;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\WithPagination;

class ArticleList extends Component
{
    use WithPagination;

    public int $perPage = 25;
    public string $search = '';
    public string $sortDirection = 'DESC';
    public string $sortColumn = 'created_at';
    public string $checkerColumn = '';
    public bool $articleForm = false;

    public function doSort($column): void
    {
        if ($this->checkerColumn == $column) {
            $this->sortDirection = $this->sortDirection == 'ASC' ? 'DESC' : 'ASC';
            return;
        }

        $this->reset('sortDirection');
        if ($column === 'name' || $column === 'email') {
            $this->sortColumn = 'users.' . $column;
        } else {
            $this->sortColumn = 'articles.' . $column;
        }
        $this->checkerColumn = $column;
    }

    public function updatedPerPage(): void
    {
        $this->resetPage();
    }

    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    #[On('article-created')]
    public function toggleModal(): void
    {
        $this->articleForm = !$this->articleForm;
    }

    public function render(): View
    {
        $articles = Article::detail()
            ->join('users', 'users.id', '=', 'articles.author_id')
            ->search($this->search)
            ->orderBy($this->sortColumn, $this->sortDirection)
            ->paginate($this->perPage);

        return view('livewire.article-list', [
            'articles' => $articles,
        ]);
    }
}
