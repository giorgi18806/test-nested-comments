<?php

namespace App\Traits;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasComments
{
    public function scopeWithComments(Builder $query): void
    {
        $query
            ->with(['comments', 'comments.user', 'comments.comments', 'comments.comments.user'])
            ->withCount('comments');
    }

    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable')
            ->latest();
    }
}
