<?php

namespace App\Models;

use App\Traits\HasComments;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Article extends Model
{
    use HasFactory;
    use HasComments;


    protected $fillable = [
        'title',
        'author_id',
        'description',
        'thumbnail',
        'slug',
        'is_published',
    ];

    protected $casts = [
        'is_published' => 'bool',
        'created_at' => 'datetime',
    ];

    public function scopeDetail(Builder $query): void
    {
        $query->with(['author'])
            ->withComments()
//            ->withLikes()
            ->published()
        ;
    }

    public function scopePublished(Builder $query): void
    {
        $query->whereIsPublished(true);
    }

    public function scopeSearch($query, $value): void
    {
        $query->where('title', 'like', "%{$value}%")
            ->orWhereHas('author', function ($query) use ($value) {
                $query->where('name', 'like', "%{$value}%");
            })
            ->orWhereHas('author', function ($query) use ($value) {
                $query->where('email', 'like', "%{$value}%");
            });
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
