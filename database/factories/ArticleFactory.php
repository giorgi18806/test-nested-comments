<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $title = ucfirst(fake()->words(2, true));
        return [
            'title' => $title,
            'author_id' => User::inRandomOrder()->first(),
            'description' => fake()->realTextBetween($minNbChars = 250, $maxNbChars = 500, $indexSize = 2),
            'slug' => Str::slug($title),
            'is_published' => fake()->boolean(),
            'created_at' => fake()->dateTimeThisYear(),
        ];
    }
}
