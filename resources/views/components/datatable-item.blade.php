<div class="flex items-center justify-between">
    {{ $columnName }}
    @if($sortColumn !== $columnName)
        <div>
            <i class="fas fa-long-arrow-alt-up gray-arrows"></i>
            <i class="fas fa-long-arrow-alt-down gray-arrows"></i>
        </div>
    @elseif($sortDirection === 'DESC')
        <i class="fas fa-long-arrow-alt-down"></i>
    @else
        <i class="fas fa-long-arrow-alt-up"></i>
    @endif
</div>
