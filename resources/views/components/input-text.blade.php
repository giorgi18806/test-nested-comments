@props(['name'])

<input type="text" name="{{ $name }}" {{ $attributes }} class="form-control mb-3">

<div>
    @error($name) <span class="error">{{ $message }}</span> @enderror
</div>
