<div wire:key="{{ $comment->id }}" class="card card--bordered flex items-center space-x-4 grow my-4" style="margin-left: {{ $loop->depth * 50}}px;">
        <div class="card-header flex items-center justify-between w-100">
            <img src="https://ui-avatars.com/api/?name={{ $comment->user->name }}" alt="{{ $comment->user->name }}">
            <div class="font-bold ml-3">{{ $comment->user->name }}</div>
            <div class="text-xs font-normal">{{ $comment->created_at->format('d.m.Y H:i') }}</div>
        </div>
        <div class="text-lg my-2 break-words align-self-start">{{ $comment->text }}</div>

        @if($comment->comment_id === null)
            <div class="text-sm m-2 align-self-end">
                <button class="btn btn-dark" wire:click="answer({{$comment->id}})">
                    Ответить
                </button>
            </div>
        @endif
</div>
