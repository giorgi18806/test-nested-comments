<form wire:submit="save">
    @csrf
    <div class="modal-content">
        <div class="modal-header">
            <h1 class="modal-title fs-5" id="exampleModalLabel">Create New Article</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <label>Title</label>
            <input type="text" wire:model="form.title" class="form-control mb-3">
            <div>
                @error('form.title') <span class="error"><small>{{ $message }}</small></span> @enderror
            </div>

            <label>Description</label>
            <textarea type="text" wire:model="form.description" class="form-control mb-3"></textarea>
            <div>
                @error('form.description') <span class="error"><small>{{ $message }}</small></span> @enderror
            </div>
            <input type="checkbox" wire:model="form.checkbox" class="form-check-input" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault"> Fill in the fields automatically</label>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </div>
</form>
