<div>
    <section class="mt-10">
        <div class="mx-auto max-w-screen-xl px-4 lg:px-12">
            <div class="bg-white dark:bg-gray-800 overflow-hidden">
                <div class="flex items-center justify-between d p-4">
                    <div class="flex">
                        <div class="relative w-full">
                            <input wire:model.live.debounce.300ms="search" type="text" class="bg-gray-50 border-solid border-4 border-grey-600" placeholder="Search" required="">
                        </div>
                    </div>
                    <div>
{{--                        <button type="button" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" data-bs-toggle="modal" data-bs-target="#exampleModal">--}}
{{--                            Create Article--}}
{{--                        </button>--}}
                        <button wire:click="toggleModal" type="button" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                            Create Article
                        </button>
                    </div>
                </div>

                <div class="overflow-x-auto">
                    <table class="w-full text-sm text-left text-gray-500">
                        <thead class="text-xs text-gray-700 uppercase">
                            <tr>
                                <th wire:click="doSort('title')" class="px-4 py-3">
                                    <x-datatable-item :sortColumn="$checkerColumn" :sortDirection="$sortDirection" columnName="title"></x-datatable-item>
                                </th>
                                <th wire:click="doSort('name')" class="px-4 py-3">
                                    <x-datatable-item :sortColumn="$checkerColumn" :sortDirection="$sortDirection" columnName="name"></x-datatable-item>
                                </th>
                                <th wire:click="doSort('email')" class="px-4 py-3">
                                    <x-datatable-item :sortColumn="$checkerColumn" :sortDirection="$sortDirection" columnName="email"></x-datatable-item>
                                </th>
                                <th wire:click="doSort('created_at')" class="px-4 py-3">
                                    <x-datatable-item :sortColumn="$checkerColumn" :sortDirection="$sortDirection" columnName="created_at"></x-datatable-item>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($articles as $article)
                                <tr class="border-b dark:border-gray-700">
                                    <td class="px-4 py-3"><a href="{{ route('show', $article) }}">{{ $article->title }}</a></td>
                                    <td class="px-4 py-3">{{ $article->author->name }}</td>
                                    <td class="px-4 py-3">{{ $article->author->email }}</td>
                                    <td class="px-4 py-3">{{ $article->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="py-4 px-3">
                    <div class="flex">
                        <div class="flex spase-x-4 items-center mb-3">
                            <label>Per Page</label>
                            <select wire:model.live="perPage" class="border-solid border-4 border-grey-600 mx-2 px-3">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                    </div>
                    {{ $articles->links() }}
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade {{ $articleForm ? 'show' : 'hide' }} id="exampleModal" >
        <div class="modal-dialog">
            <livewire:create-article/>
        </div>
    </div>
</div>
