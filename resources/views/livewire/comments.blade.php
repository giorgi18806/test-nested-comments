<div>
    <div>{{ $form->commentId ? 'Написать ответ на комментарий' : 'Комментарии' }}</div>

    <form name="comments" wire:submit="store">
        @csrf
        <div>
            <textarea name="message" wire:model="form.message" class="form-control" id="textarea"></textarea>
            @error('form.message') <span class="error">{{ $message }}</span> @enderror
        </div>

        <div class="recaptcha-wrapper">
            @if(config('services.recaptcha.key'))
{{--                @dd('giorgi', config('services.recaptcha.key'))--}}
                <div class="g-recaptcha " data-sitekey="{{ config('services.recaptcha.key') }}"></div>
            @endif
            @error('g-recaptcha-response')
            <div class="recaptcha-error" id="error-captcha">{{ $message }}</div>
            @enderror
        </div>

        <button type="submit" class="btn btn-dark mt-1">
            {{ $form->commentId ? 'Ответить' : 'Написать' }}
        </button>

        @if($form->commentId)
            <button wire:click="cancel" class="btn btn-dark">
                Отменить
            </button>
        @endif
    </form>

    @foreach($comments as $comment)
        @include('livewire.shared.comment', ['comment' => $comment])

        @if($comment->comments->isNotEmpty())

            @foreach($comment->comments as $child)
                @include('livewire.shared.comment', ['comment' => $child])
            @endforeach
        @endif
    @endforeach
    <hr class="my-5">
</div>

@push('scripts')
    <script>
        document.addEventListener('livewire:init', () => {
            Livewire.on('change-focus', (event) => {
                $("#textarea").focus();
            });
        });
    </script>
@endpush
