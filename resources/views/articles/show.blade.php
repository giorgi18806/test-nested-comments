@extends('layouts.app')

@section('content')
    <div class="flex justify-content-between">
        <div class="card card--bordered mb-4 col-sm-8">
            <div class="card-header bg-dark text-light">
                {{ $article->title }}
            </div>
            <div class="card-body">
                {{ $article->description }}
            </div>
        </div>
        <div>
            <a href="{{ route('home') }}" class="btn btn-dark text-white">Back</a>
        </div>
    </div>
    <livewire:comments :model="$article" />
@endsection
